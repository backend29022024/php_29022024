<?php
include 'include/header.php';
?>

<?php 
    $loginCheck = Session::get('customerLogin');
    if($loginCheck) {
        echo "<script>window.location.href='checkout.php'</script>";
    }

    if($_SERVER['REQUEST_METHOD']==="POST" && isset($_POST['submit'])) {
        $insertCustomer = $customer->insertCustomer($_POST);
    }

    if($_SERVER['REQUEST_METHOD']==="POST" && isset($_POST['login'])) {
        $loginCustomer = $customer->loginCustomer($_POST);
    }

?>

<div class="main container">
    <div class="content">
        <div class="login_panel">
            <h3>Existing Customers</h3>
            <?php 
                if(isset($loginCustomer)){
                    echo $loginCustomer;
                }
            ?>
            <p>Sign in with the form below.</p>
            <form action="" method="post">
                <input type="email" name="email" class="field" placeholder="Enter email">
                <input type="password" name="passwd" class="field" placeholder="Enter password">
                <p class="note">If you forgot your passoword just enter your email and click <a href="#">here</a></p>
                <div class="buttons">
                    <div><input name="login" type="submit" class="grey" value="Sign In" /></div>
                </div>
            </form>

        </div>


        <div class="register_account">
            <h3>Register New Account</h3>
            <?php 
                if(isset($insertCustomer)) {
                    echo $insertCustomer;
                }
            ?>
            <form method="POST" action="">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div>
                                    <input type="text" name="name" placeholder="Enter name">
                                </div>

                                <div>
                                    <input type="text" name="city" placeholder="Enter city">
                                </div>

                                <div>
                                    <input type="text" name="zipcode" placeholder="Enter zip code">
                                </div>
                                <div>
                                    <input type="email" name="email" placeholder="Enter email">
                                </div>
                            </td>
                            <td>
                                <div>
                                    <input type="text" name="address" placeholder="Enter address">
                                </div>
                                <div>
                                    <select id="country" name="country" onchange="change_country(this.value)"
                                        class="frm-field required">
                                        <option value="null">Select a Country</option>
                                        <option value="AF">Afghanistan</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>

                                    </select>
                                </div>

                                <div>
                                    <input type="text" name="phone" placeholder="Enter phone">
                                </div>

                                <div>
                                    <input type="password" name="passwd" placeholder="Enter password">
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="search">
                    <div><input name="submit" type="submit" class="grey" value="Create" /></div>
                </div>
                <p class="terms">By clicking 'Create Account' you agree to the <a href="#">Terms &amp; Conditions</a>.
                </p>
                <div class="clear"></div>
            </form>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php
include 'include/footer.php';
?>