<?php
    include_once "include/header.php";
?>

<?php 
  

    $loginCheck = Session::get("customerLogin");
    if($loginCheck==false) {
        echo "<script>window.location.href='login.php'</script>";
    }

    
    if (isset($_GET['confirmID'])) {
        $id = $_GET['confirmID'];
		$price = $_GET['price'];
		$time = $_GET['time'];
		$confirm = $cart->confirm($id, $price, $time);
    }

?>


<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="#">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Shopping Cart</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Cart Start -->
<div class="container-fluid">

    <div class="row px-xl-5">

        <div class="col-lg-8 table-responsive mb-5">
            <table class="table table-light table-borderless table-hover text-center mb-0">
                <?php 
                    if(isset($updateQuantity)){
                        echo $updateQuantity;
                    }
                    if(isset($deleteCart)) {
                        echo $deleteCart;
                    }
                ?>


                <thead class="thead-dark">
                    <tr>
                        <th>Products</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Date Order</th>
                        <th>Status</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody class="align-middle">

                    <?php 
                        $id = Session::get("customerID");
                        $getOrder = $cart->getOrder($id);
                        if($getOrder) {
                            while($result=$getOrder->fetch_assoc()) {
                                ?>
                    <tr>
                        <td class="align-middle"><?php echo $result['productName']?>
                        </td>
                        <td><img src="admin/upload/<?php echo $result['image']?>" alt="" style="width: 50px;"></td>
                        <td class="align-middle"><?php echo $result['price']?></td>
                        <td class="align-middle">
                            <!-- <div class="input-group quantity mx-auto" style="width: 100px;">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-minus">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <input type="text"
                                    class="form-control form-control-sm bg-secondary border-0 text-center" value="1">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-plus">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div> -->
                            <?php echo $result['quantity']?>

                        </td>
                        <td class="align-middle">
                            <?php echo $format->formatDate($result['quantity'])?>
                        </td>
                        <td> <?php
                            if ($result['status']=='0') {
                                echo 'Pending'; 
                            } else if($result['status']=='1') {
                                ?>
                            <span>Shifted</span>


                            <?php
                            } else {
                                echo 'Recieved';
                            }
                        ?>
                        </td>
                        <td class="align-middle">
                            <?php 
                                if ($result['status']=='0') {
                                    echo '<button class="btn btn-sm btn-danger">
                                    <a href="?cartID='.$result['id'].'">CANCEL</a>
                                </button>'; 
                                } else if ($result['status']=='1') {
                                    ?>
                            <a href="?confirmID=<?php echo $id?>&price=<?php echo $result['price']?>
                                &time=<?php echo $result['date_order']?>">confirm</a>
                            <?php
                                }else {
                                    echo 'CANCEL';
                                }
                            ?>

                        </td>
                    </tr>

                    <?php
                            }
                        }
                    ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- Cart End -->


<?php
    include_once "include/footer.php";
?>