<?php
    include_once "include/header.php";
?>

<?php 
    $loginCheck = Session::get('customerLogin');
    if(!$loginCheck) {
        echo "<script>window.location.href='login.php'</script>";
    }
?>

<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="index.php">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">History order</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<div class="container">
    <div>
        <h4>mm/dd/yyyy</h4>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Image</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                </tr>

            </tbody>
        </table>
        <div class="d-flex">
            <button type="button" class="btn btn-success">Revieced</button>
        </div>
    </div>
</div>

<?php
    include_once "include/footer.php";
?>