<?php
    include_once "include/header.php";
?>

<?php 
    $loginCheck = Session::get('customerLogin');
    if(!$loginCheck) {
        echo "<script>window.location.href='login.php'</script>";
    }
?>

<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="index.php">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Profile</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Checkout Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <table>
            <?php 
                $id = Session::get("customerID");
                $showCustomer = $customer->showCustomer($id);
                if($customer) {
                    while ($result=$showCustomer->fetch_assoc()) {
                        ?>
            <tr>
                <td>Name</td>
                <td>:</td>
                <td><?php echo $result['name']?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td><?php echo $result['email']?></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>:</td>
                <td><?php echo $result['phone']?></td>
            </tr>
            <tr>
                <td>Adress</td>
                <td>:</td>
                <td><?php echo $result['address']?></td>
            </tr>
            <tr>
                <td>City</td>
                <td>:</td>
                <td><?php echo $result['city']?></td>
            </tr>
            <tr>
                <td>Country</td>
                <td>:</td>
                <td><?php echo $result['country']?></td>
            </tr>
            <tr>
                <td>Zip Code</td>
                <td>:</td>
                <td><?php echo $result['zipcode']?></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td colspan="3"><a href="editprofile.php">Edit</a></td>

            </tr>
            <?php
                    }
                }
            ?>

        </table>
    </div>
</div>
<!-- Checkout End -->

<?php
    include_once "include/footer.php";
?>