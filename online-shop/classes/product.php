<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Product {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function insertProduct($data, $files) {

        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $brandID = mysqli_real_escape_string($this->db->link, $data['brandID']);
        $categoryID = mysqli_real_escape_string($this->db->link, $data['categoryID']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);
        $quantity = mysqli_real_escape_string($this->db->link, $data['quantity']);
        $description = mysqli_real_escape_string($this->db->link, $data['description']);

        $permited = array('jpg','jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $dir = explode('.', $file_name);
        $file_ext = strtolower(end($dir));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;

        if ($productName=="" || $brandID==""|| $quantity==''|| $categoryID==""|| $type==""|| $description==""|| $price=="") {
            $alert = "<span class='error'>Fields must be not empty</span>";
            return $alert;
        } else {
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "INSERT INTO product(productName, product_quantity, categoryId, brandId, description,price, image, type)
                VALUES ('$productName', '$quantity', '$categoryID', '$brandID', '$description', '$price', '$unique_image', '$type')";
            $result = $this->db->insert($query);
            if ($result) {
                $alert= "<span class='success'>Insert Product successfully</span>";
                return $alert;
            } else {
                $alert= "<span class='error'>Insert Product failed</span>";
                return $alert;
            }
        }
    }

    public function showProduct() {
        $query = "SELECT p.*, c.categoryName, b.brandName
            FROM product as p, category as c, brand as b
            WHERE p.categoryID = c.id AND p.brandID = b.id";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateProduct($data, $files, $id) {
        $productName = mysqli_real_escape_string($this->db->link, $data['productName']);
        $brand = mysqli_real_escape_string($this->db->link, $data['brandID']);
        $quantity = mysqli_real_escape_string($this->db->link, $data['quantity']);
        $category = mysqli_real_escape_string($this->db->link, $data['categoryID']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);
        $desc = mysqli_real_escape_string($this->db->link, $data['description']);
        $price = mysqli_real_escape_string($this->db->link, $data['price']);

        $permited = array('jpg', 'jpeg', 'png', 'gìf');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $dir = explode('.', $file_name);
        $file_ext = strtolower(end($dir));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;



        if ($productName=="" || $brand==""|| $quantity==""| $category==""|| $type==""|| $desc==""|| $price=="") {
            $alert = "<span class='error'>Product name must be not empty</span>";
            return $alert;
        } else {
            if (!empty($file_name)) {
                 if (in_array($file_ext, $permited)===false) {
                    $alert =  "<span>You can upload only:.".implode(', ', $permited)."</span>";
                    return $alert;
                }

                move_uploaded_file($file_temp, $uploaded_image);
                $query = "UPDATE product SET
                productName = '$productName',
                product_quantity = '$quantity',
                categoryID = '$category',
                brandID = '$brand',
                description = '$desc',
                price = '$price',
                image = '$unique_image',
                type = '$type'
                WHERE id = '$id'";
            } else {
                $query = "UPDATE product SET
                productName = '$productName',
                product_quantity = '$quantity',
                categoryID = '$category',
                brandID = '$brand',
                description = '$desc',
                price = '$price',
                type = '$type'
                WHERE id = '$id'";
            }

            $result = $this->db->update($query);
            if ($result) {
                $alert= "<span class='success'>Update Product successfully</span>";
                return $alert;
            } else {
                $alert= "<span class='error'>Update Product failed</span>";
                return $alert;
            }
        }
    }

    public function getProductById($id) {
        $query = "SELECT * FROM product WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deleteProduct($id) {
        $query = "DELETE FROM product WHERE id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $alert = "<span class='success'>product deleted successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>product deleted not successfully</span>";
                return $alert;
        }
    }


    // FE

    public function getProductFeatured() {
        $query = "SELECT * FROM product WHERE type = '1'";
        $result = $this->db->select($query);
        return $result;
    }

    public function getProductNew() {
        $query = "SELECT * FROM product ORDER BY id LIMIT 4";
        $result = $this->db->select($query);
        return $result;
    }

    public function getProductPage() {
        $eachPage = 3;
        if(isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $limitPage = ($page-1)*$eachPage;
        $query = "SELECT * FROM product ORDER BY id desc LIMIT $limitPage,$eachPage";
        $result = $this->db->select($query);
        return $result;
    }

    public function getAllProduct() {
        $query = "SELECT * FROM product ORDER BY id";
        $result = $this->db->select($query);
        return $result;
    }

    public function getDetail($id) {
        $query = "SELECT p.*, c.categoryName, b.brandName
            FROM product as p INNER JOIN category as c ON p.categoryID =c.id
             INNER JOIN brand as b ON b.id = p.brandID
            WHERE p.id = '$id'";

        $result = $this->db->select($query);
        return $result;
    }

    public function insertCompare($productID, $customerID) {
        $productID = mysqli_real_escape_string($this->db->link, $productID);
        $customerID = mysqli_real_escape_string($this->db->link, $customerID);


        $query = "SELECT * FROM product WHERE id = '$productID'";
        $result = $this->db->select($query)->fetch_assoc();
        $name = $result['productName'];
        $price = $result['price'];
        $image = $result['image'];

        $check = "SELECT * FROM compare WHERE productID = '$productID' AND customerID = '$customerID'";
        $check = $this->db->select($check);
        if($check) {
            $msg = "<span class='error'>Product Already Added</span>";
            return $msg;
        } else {
            $query_compare = "INSERT INTO compare(productID, customerID, productName, price, image)
            VALUES('$productID', '$customerID', '$name', '$price', '$image')";
            $insert_compare = $this->db->insert($query_compare);
            if ($insert_compare) {
                $alert =  "<span>Successfully</span>";
                return $alert;
            } else {
                $alert =  "<span>Failed</span>";
                return $alert;
            }
        }


        
    }

    public function getCompare($id) {
        $query = "SELECT * FROM compare WHERE customerID = '$id'";
        $result = $this->db->select($query);
        return $result;    
    }

    public function deleteCompare($id) {
        $query = "DELETE FROM compare WHERE productID = '$id'";
        $result = $this->db->delete($query);
        return $result;
    }

    public function deleteAllCompare($id) {
        $query = "DELETE FROM compare WHERE customerID = '$id'";
        $result = $this->db->delete($query);
        return $result;
    }

    public function insertWishlist($productID, $customerID) {
        $productID = mysqli_real_escape_string($this->db->link, $productID);
        $customerID = mysqli_real_escape_string($this->db->link, $customerID);


        $query = "SELECT * FROM product WHERE id = '$productID'";
        $result = $this->db->select($query)->fetch_assoc();
        $name = $result['productName'];
        $price = $result['price'];
        $image = $result['image'];

        $check = "SELECT * FROM wishlist WHERE productID = '$productID' AND customerID = '$customerID'";
        $check = $this->db->select($check);
        if($check) {
            $msg = "<span class='error'>Product Already Added</span>";
            return $msg;
        } else {
            $query_wishlist = "INSERT INTO wishlist(productID, customerID, productName, price, image)
            VALUES('$productID', '$customerID', '$name', '$price', '$image')";
            $insert_wishlist = $this->db->insert($query_wishlist);
            if ($insert_wishlist) {
                $alert =  "<span>Successfully</span>";
                return $alert;
            } else {
                $alert =  "<span>Failed</span>";
                return $alert;
            }
        }


        
    }

    public function getWishlist($id) {
        $query = "SELECT * FROM wishlist WHERE customerID = '$id'";
        $result = $this->db->select($query);
        return $result;    
    }

    public function deleteWishlist($id, $customerID) {
        $query = "DELETE FROM wishlist WHERE productID = '$id' AND customerID='$customerID'";
        $result = $this->db->delete($query);
        return $result;
    }

    public function deleteAllWishlist($id) {
        $query = "DELETE FROM wishlist WHERE customerID = '$id'";
        $result = $this->db->delete($query);
        return $result;
    }


    public function insertSlider($data, $file) {
        $sliderName = mysqli_real_escape_string($this->db->link, $data['sliderName']);
        $type = mysqli_real_escape_string($this->db->link, $data['type']);

        $permited = array('jpg', 'jpeg', 'png', 'gìf');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $dir = explode('.', $file_name);
        $file_ext = strtolower(end($dir));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;



        if ($sliderName=="" ) {
            $alert = "<span class='error'>Slider name must be not empty</span>";
            return $alert;
        } else {
            if (!empty($file_name)) {
                if ($file_size>400000048) {
                    $alert =  "<span>Image size should be less than 2MB</span>";
                    return $alert;
                } else if (in_array($file_ext, $permited)===false) {
                    $alert =  "<span>You can upload only:.".implode(',', $permited)."</span>";
                    return $alert;
                }

                move_uploaded_file($file_temp, $uploaded_image);
                $query = "INSERT INTO slider(sliderName, image, type)
                    VALUES ('$sliderName', '$unique_image', '$type')";
                $result = $this->db->insert($query);
                if ($result) {
                    $alert= "<span class='success'>Insert Slider successfully</span>";
                    return $alert;
                } else {
                    $alert= "<span class='error'>Insert Slider failed</span>";
                    return $alert;
                }
            }

            
        }
    }

    public function showSlider() {
        $query = "SELECT * FROM slider";
        $result = $this->db->select($query);
        return $result;
    }

    

    public function getSliderOn() {
        $query = "SELECT * FROM slider WHERE type ='1'";
        $result = $this->db->select($query);
        return $result;
    }


    
    public function searchProduct($key) {
        $query = "SELECT * FROM product WHERE productName LIKE '%$key%'";
        $result = $this->db->select($query);
        return $result;
    }

}