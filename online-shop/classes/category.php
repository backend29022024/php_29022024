<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Category {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function insertCategory($categoryName) {
        $categoryName = $this->format->validation($categoryName);

        $categoryName = mysqli_real_escape_string($this->db->link, $categoryName);

        if(empty($categoryName)) {
            $alert = "<span class='error'>Category name must be not empty!</span>";
            return $alert;
        } else {
            $query = "INSERT INTO category(categoryName) VALUES('$categoryName')";
            $result = $this->db->insert($query);
            if($result) {
                $alert = "<span class='success'>Insert category successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Insert category not successfully</span>";
                return $alert;
            }
        }
    }

    public function showCategory() {
        $query = "SELECT * FROM category";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateCategory($id, $categoryName) {
        $categoryName = $this->format->validation($categoryName);

        $categoryName = mysqli_real_escape_string($this->db->link, $categoryName);
        $id = mysqli_real_escape_string($this->db->link, $id);

        if(empty($categoryName)) {
            $alert = "<span class='error'>Category name must be not empty!</span>";
            return $alert;
        } else {
            $query = "UPDATE category SET categoryName = '$categoryName' WHERE id = '$id'";
            $result = $this->db->update($query);
            if($result) {
                $alert = "<span class='success'>Update category successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Update category not successfully</span>";
                return $alert;
            }
        }
    }

    public function getCategoryById($id) {
        $query = "SELECT * FROM category WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deleteCategory($id) {
        $query = "DELETE FROM category WHERE id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $alert = "<span class='success'>Category deleted successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Category deleted not successfully</span>";
                return $alert;
        }
    }

    //FE

    public function getAllCategory() {
        $query = "SELECT * FROM category";
        $result = $this->db->select($query);
        return $result;
    }

    
}