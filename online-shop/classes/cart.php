<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Cart {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function addCart($id, $quantity, $stock) {
        $id = mysqli_real_escape_string($this->db->link, $id);
        $quantity = mysqli_real_escape_string($this->db->link, $quantity);
        $stock = mysqli_real_escape_string($this->db->link, $stock);
        $sessionId = session_id();

        $query = "SELECT * FROM product WHERE id = '$id'";
        $result = $this->db->select($query)->fetch_assoc();
        $name = $result['productName'];
        $price = $result['price'];
        $image = $result['image'];

        if($quantity < $stock) {
            $check = "SELECT * FROM cart WHERE productID = '$id' AND sessionID = '$sessionId'";
            $check = $this->db->select($check);
            if($check) {
                $msg = "<span class='error'>Product Already Added</span>";
                return $msg;
            } else {
                $query_cart = "INSERT INTO cart(productID, sessionID, productName, price, stock, quantity, image)
                VALUES('$id', '$sessionId', '$name', '$price','$stock', '$quantity', '$image')";
                $insert_cart = $this->db->insert($query_cart);
                if ($insert_cart) {
                    echo "<script>window.location.href='cart.php'</script>";
                } else {
                    echo "<script>window.location.href='404.php'</script>";
                }
            }
        } else {
            $msg = "<span class='error'>Insufficient product inventory</span>";
                return $msg;
        }
        
    }

    public function getCart() {
        $sessionId = session_id();
        $query = "SELECT * FROM cart WHERE sessionID = '$sessionId'";
        $result = $this->db->select($query);
        return $result;
    }

    public function getCartCheckBuy() {
        $sessionId = session_id();
        $query = "SELECT * FROM cart WHERE sessionID = '$sessionId' AND check_buy = 1";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateQuantity($id, $quantity, $stock) {
        $quantity = mysqli_real_escape_string($this->db->link, $quantity);
        $stock = mysqli_real_escape_string($this->db->link, $stock);
        $id = mysqli_real_escape_string($this->db->link, $id);
        if($stock > $quantity) {
            $query = "UPDATE cart SET quantity = '$quantity' WHERE id = '$id'";
            $result = $this->db->update($query);
            if($result) {
                $msg = "<span class='success'>Product quantity updated successfully</span>";
                return $msg;
            } else {
                $msg = "<span class='success'>Product quantity updated not successfully</span>";
                return $msg;
            }
        } else {
            $msg = "<span class='success'>Insufficient product inventory</span>";
                return $msg;
        }
        
    }

    public function deleteCart($id) {
        $cartId = mysqli_real_escape_string($this->db->link, $id);
        $query = "DELETE FROM cart WHERE id = '$cartId'";
        $result = $this->db->delete($query);
        if (!$result){
            $msg = "<span>Product deleted not successfully</span>";
            return $msg;
        }
    }

    public function checkCart() {
        $sessionId = session_id();
        $query = "SELECT * FROM cart WHERE sessionID = '$sessionId'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deleteAllCart() {
        $sessionId = session_id();
        $query = "DELETE FROM cart WHERE sessionID = '$sessionId'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deleteAllCartOrder() {
        $sessionId = session_id();
        $query = "DELETE FROM cart WHERE sessionID = '$sessionId' AND check_buy=1";
        $result = $this->db->select($query);
        return $result;
    }

    public function insertOrder($id) {
        $sessionId = session_id();
        $query = "SELECT * FROM cart WHERE sessionID = '$sessionId'";
        $getProduct = $this->db->select($query);
        $order_code = md5(rand(00,9999));
        $queryPlace = "INSERT INTO placed(order_code, status, customer_id)
                VALUES('$order_code', '0', '$id')";
        $insertPlace = $this->db->insert($queryPlace);
        if($getProduct) {

            while($result = $getProduct->fetch_assoc()) {
                $productID = $result['productID'];
                $productName = $result['productName'];
                $quantity = $result['quantity'];
                $image = $result['image'];
                $price = $result['price']*$quantity;
                $customerID = $id;
                $queryOrder = "INSERT INTO table_order(productID,order_code , productName, customerID, quantity, price, image)
                VALUES('$productID','$order_code', '$productName', '$customerID', '$quantity', '$price', '$image')";
                $insertOrder = $this->db->insert($queryOrder);
                // if ($insertOrder) {
                //     echo "<script>window.location.href='cart.php'</script>";
                // } else {
                //     echo "<script>window.location.href='404.php'</script>";
                // }
            }
        }
    }

    
    public function getAmountPrice($id) {
        $query = "SELECT * FROM table_order WHERE customerID = '$id'";
        $result = $this->db->select($query);
        return $result;
    }


    public function getOrder($id) {
        $query = "SELECT * FROM table_order WHERE customerID = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    
    public function checkOrder($id) {
        $query = "SELECT * FROM table_order WHERE customerID = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function showOrder($code) {
        $query = "SELECT * FROM table_order WHERE order_code = '$code'";
        $result = $this->db->select($query);
        return $result;
    }

    public function getInboxCart() {
        $query = "SELECT * FROM placed ORDER BY order_created";
        $result = $this->db->select($query);
        return $result;
    }

    public function shifted($id) {
        $id = mysqli_real_escape_string($this->db->link, $id);

        $query = "UPDATE table_order SET status = '1' WHERE order_code = '$id'";
        $result = $this->db->update($query);
        if($result) {
            $msg = "<span class='success'>Updat order successfully</span>";
            return $msg;
        } else {
            $msg = "<span class='success'>Updat order not successfully</span>";
            return $msg;
        }
    }

    public function deleteShiftID($id) {
        $id = mysqli_real_escape_string($this->db->link, $id);

        $query = "DELETE FROM table_order WHERE order_code = '$id'";
        $result = $this->db->delete($query);
        if($result) {
            $msg = "<span class='success'>Delete order successfully</span>";
            return $msg;
        } else {
            $msg = "<span class='success'>Delete order not successfully</span>";
            return $msg;
        }
    }

    public function confirm($id, $price, $time) {
        $time = mysqli_real_escape_string($this->db->link, $time);
        $id = mysqli_real_escape_string($this->db->link, $id);
        $price = mysqli_real_escape_string($this->db->link, $price);

        $query = "UPDATE table_order SET status = '2' WHERE customerID = '$id' AND date_order='$time' AND price ='$price'";
        $result = $this->db->update($query);
        return $result;
    }
   
}