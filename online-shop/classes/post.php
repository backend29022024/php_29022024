<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Post {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function insertPost($title, $description, $status) {
        $title = $this->format->validation($title);
        $description = $this->format->validation($description);
        $status = $this->format->validation($status);

        $title = mysqli_real_escape_string($this->db->link, $title);
        $description = mysqli_real_escape_string($this->db->link, $description);
        $status = mysqli_real_escape_string($this->db->link, $status);
        

        if($title =='' ||$description =='' || $status=='') {
            $alert = "<span class='error'>post name must be not empty!</span>";
            return $alert;
        } else {
            $query = "INSERT INTO category_post(title, description, status) VALUES('$title', '$description', '$status')";
            $result = $this->db->insert($query);
            if($result) {
                $alert = "<span class='success'>Insert post successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Insert post not successfully</span>";
                return $alert;
            }
        }
    }

    public function showPost() {
        $query = "SELECT * FROM category_post";
        $result = $this->db->select($query);
        return $result;
    }

    public function updatePost($id, $title, $description, $status) {
        $title = $this->format->validation($title);
        $description = $this->format->validation($description);
        $status = $this->format->validation($status);

        $title = mysqli_real_escape_string($this->db->link, $title);
        $description = mysqli_real_escape_string($this->db->link, $description);
        $status = mysqli_real_escape_string($this->db->link, $status);
        $id = mysqli_real_escape_string($this->db->link, $id);

        if($title =='' ||$description =='' || $status=='') {
            $alert = "<span class='error'>post name must be not empty!</span>";
            return $alert;
        } else {
            $query = "UPDATE category_post SET title='$title', description='$description', status='$status' WHERE id = '$id'";
            $result = $this->db->update($query);
            if($result) {
                $alert = "<span class='success'>Update post successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Update post not successfully</span>";
                return $alert;
            }
        }
    }

    public function getPostByID($id) {
        $query = "SELECT * FROM category_post WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deletePost($id) {
        $query = "DELETE FROM category_post WHERE id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $alert = "<span class='success'>post deleted successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>post deleted not successfully</span>";
                return $alert;
        }
    }

    //FE

    public function getAllpost() {
        $query = "SELECT * FROM post";
        $result = $this->db->select($query);
        return $result;
    }

    
}