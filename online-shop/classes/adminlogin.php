<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");
include_once ($filepath."/../library/session.php");
Session::checkLogin();

class AdminLogin {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function loginAdmin($username, $passwd) {
        $username = $this->format->validation($username);
        $passwd = $this->format->validation($passwd);

        $username = mysqli_real_escape_string($this->db->link, $username);
        $passwd = mysqli_real_escape_string($this->db->link, $passwd);

        if(empty($username) || empty($passwd)) {
            $alert = "User and Passwd must be not empty!";
            return $alert;
        } else {
            $query = "SELECT * FROM admin WHERE username = '$username' AND passwd = '$passwd' LIMIT 1";
            $result = $this->db->select($query);
            if($result) {
                $value = $result->fetch_assoc();
                Session::set('adminLogin', true);
                Session::set('adminID', $value['id']);
                Session::set('adminName', $value['name']);
                Session::set('adminUser', $value['username']);
                header('Location:index.php');
            } else {
                $alert = "User or Passwd not pass!";
                return $alert;
            }
        }
    }
}