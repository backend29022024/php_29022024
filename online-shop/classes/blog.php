<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Blog {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function insertBlog($data, $files) {

        $title = mysqli_real_escape_string($this->db->link, $data['title']);
        $description = mysqli_real_escape_string($this->db->link, $data['description']);
        $content = mysqli_real_escape_string($this->db->link, $data['content']);
        $status = mysqli_real_escape_string($this->db->link, $data['status']);
        $post = mysqli_real_escape_string($this->db->link, $data['post']);

        $permited = array('jpg','jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $dir = explode('.', $file_name);
        $file_ext = strtolower(end($dir));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;

        if ($title=="" || $description==""|| $post==""|| $status==""|| $content=="") {
            $alert = "<span class='error'>Fields must be not empty</span>";
            return $alert;
        } else {
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "INSERT INTO blogs(blog_title, description, content, post,image, status)
                VALUES ('$title', '$description', '$content', '$post', '$unique_image', '$status')";
            $result = $this->db->insert($query);
            if ($result) {
                $alert= "<span class='success'>Insert blog successfully</span>";
                return $alert;
            } else {
                $alert= "<span class='error'>Insert blog failed</span>";
                return $alert;
            }
        }
    }

    public function showBlog() {
        $query = "SELECT b.*, p.title
            FROM blogs as b, category_post as p
            WHERE p.id = b.post";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateBlog($data, $files, $id) {
        $title = mysqli_real_escape_string($this->db->link, $data['title']);
        $description = mysqli_real_escape_string($this->db->link, $data['description']);
        $content = mysqli_real_escape_string($this->db->link, $data['content']);
        $status = mysqli_real_escape_string($this->db->link, $data['status']);
        $post = mysqli_real_escape_string($this->db->link, $data['post']);

        $permited = array('jpg', 'jpeg', 'png', 'gìf');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];

        $dir = explode('.', $file_name);
        $file_ext = strtolower(end($dir));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;



        if ($title=="" || $description==""|| $post==""|| $status==""|| $content=="") {
            $alert = "<span class='error'>blog name must be not empty</span>";
            return $alert;
        } else {
            if (!empty($file_name)) {
                if ($file_size>2048) {
                    $alert =  "<span>Image size should be less than 2MB</span>";
                    return $alert;
                } else if (in_array($file_ext, $permited)===false) {
                    $alert =  "<span>You can upload only:.".implode(', ', $permited)."</span>";
                    return $alert;
                }

                move_uploaded_file($file_temp, $uploaded_image);
                $query = "UPDATE blogs SET
                blog_title = '$title',
                description = '$description',
                content = '$content',
                post = '$post',
                image = '$unique_image',
                status = '$status'
                WHERE id = '$id'";
            } else {
                $query = "UPDATE blogs SET
                blog_title = '$title',
                description = '$description',
                content = '$content',
                post = '$post',
                status = '$status'
                WHERE id = '$id'";
            }

            $result = $this->db->update($query);
            if ($result) {
                $alert= "<span class='success'>Update blog successfully</span>";
                return $alert;
            } else {
                $alert= "<span class='error'>Update blog failed</span>";
                return $alert;
            }
        }
    }

    public function getBlogById($id) {
        $query = "SELECT * FROM blogs WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deleteBlog($id) {
        $query = "DELETE FROM blogs WHERE id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $alert = "<span class='success'>blog deleted successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>blog deleted not successfully</span>";
                return $alert;
        }
    }


    // FE

    public function getBlogNew() {
        $query = "SELECT * FROM blogs ORDER BY id LIMIT 4";
        $result = $this->db->select($query);
        return $result;
    }


    public function getAllBlog() {
        $query = "SELECT * FROM blog ORDER BY id";
        $result = $this->db->select($query);
        return $result;
    }

    public function getDetail($id) {
        $query = "SELECT p.*, c.categoryName, b.brandName
            FROM blog as p INNER JOIN category as c ON p.categoryID =c.id
             INNER JOIN brand as b ON b.id = p.brandID
            WHERE p.id = '$id'";

        $result = $this->db->select($query);
        return $result;
    }
 
    public function searchBlog($key) {
        $query = "SELECT * FROM blogs WHERE blogName LIKE '%$key%'";
        $result = $this->db->select($query);
        return $result;
    }

}