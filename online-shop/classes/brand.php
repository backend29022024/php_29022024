<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Brand {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function insertBrand($brandName) {
        $brandName = $this->format->validation($brandName);

        $brandName = mysqli_real_escape_string($this->db->link, $brandName);

        if(empty($brandName)) {
            $alert = "<span class='error'>brand name must be not empty!</span>";
            return $alert;
        } else {
            $query = "INSERT INTO brand(brandName) VALUES('$brandName')";
            $result = $this->db->insert($query);
            if($result) {
                $alert = "<span class='success'>Insert brand successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Insert brand not successfully</span>";
                return $alert;
            }
        }
    }

    public function showBrand() {
        $query = "SELECT * FROM brand";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateBrand($id, $brandName) {
        $brandName = $this->format->validation($brandName);

        $brandName = mysqli_real_escape_string($this->db->link, $brandName);
        $id = mysqli_real_escape_string($this->db->link, $id);

        if(empty($brandName)) {
            $alert = "<span class='error'>brand name must be not empty!</span>";
            return $alert;
        } else {
            $query = "UPDATE brand SET brandName = '$brandName' WHERE id = '$id'";
            $result = $this->db->update($query);
            if($result) {
                $alert = "<span class='success'>Update brand successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>Update brand not successfully</span>";
                return $alert;
            }
        }
    }

    public function getBrandById($id) {
        $query = "SELECT * FROM brand WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function deleteBrand($id) {
        $query = "DELETE FROM brand WHERE id = '$id'";
        $result = $this->db->delete($query);
        if($result){
            $alert = "<span class='success'>brand deleted successfully</span>";
                return $alert;
            } else {
                $alert = "<span class='error'>brand deleted not successfully</span>";
                return $alert;
        }
    }
}