<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../library/database.php");
include_once ($filepath."/../helper/format.php");

class Customer {

    private $db;
    private $format;

    public function __construct() {
        $this->db = new Database();
        $this->format = new Format();
    }

    public function insertCustomer($data) {
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $passwd = mysqli_real_escape_string($this->db->link, md5($data['passwd']));
        $zipcode = mysqli_real_escape_string($this->db->link, $data['zipcode']);

        if ($name=="" || $address==""|| $city==""|| $phone==""|| $email==""|| $zipcode=="" || $country =="" || $passwd =="") {
            $alert = "Product name must be not empty";
            return $alert;
        } else {
            $checkEmail = "SELECT * FROM customer WHERE email='$email'";
            $checkEmail = $this->db->select($checkEmail);
            if($checkEmail){
                $msg = "<span class='error'>Email already existed</span>";
                return $msg;
            } else {
                $query = "INSERT INTO customer(name, address, city, country, zipcode, phone, email, passwd)
                VALUES ('$name', '$address', '$city', '$country', '$zipcode', '$phone', '$email', '$passwd')";
                $result = $this->db->insert($query);
                if ($result) {
                    $alert= "<span class='success'>Customer created successfully</span>";
                    return $alert;
                } else {
                    $alert= "<span class='error'>Customer created not successfully</span>";
                    return $alert;
                }
            }
            
        }
    }

    public function loginCustomer($data) {
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $passwd = mysqli_real_escape_string($this->db->link, md5($data['passwd']));
        if ($email==""|| $passwd =="") {
            $alert = "Email and Passwd must be not empty";
            return $alert;
        } else {
            $checkEmail = "SELECT * FROM customer WHERE email='$email' AND passwd='$passwd'";
            $checkEmail = $this->db->select($checkEmail);
            if($checkEmail){
            $result = $checkEmail->fetch_assoc();
                Session::set('customerLogin', true);
                Session::set('customerID', $result['id']);
                Session::set('customerName', $result['name']);
                echo "<script>window.location.href='index.php'</script>";
            } else {
                $alert= "<span class='error'>Email Or Passwd doesn't match</span>";
                return $alert;
            }
            
        }
    }

    public function showCustomer($id) {
        $query = "SELECT * FROM customer WHERE id = '$id'";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateCustomer($data, $id) {
        $name = mysqli_real_escape_string($this->db->link, $data['name']);
        $address = mysqli_real_escape_string($this->db->link, $data['address']);
        $city = mysqli_real_escape_string($this->db->link, $data['city']);
        $country = mysqli_real_escape_string($this->db->link, $data['country']);
        $email = mysqli_real_escape_string($this->db->link, $data['email']);
        $phone = mysqli_real_escape_string($this->db->link, $data['phone']);
        $zipcode = mysqli_real_escape_string($this->db->link, $data['zipcode']);

        if ($name=="" || $address==""|| $city==""|| $phone==""|| $email==""|| $zipcode=="" || $country =="") {
            $alert = "Product name must be not empty";
            return $alert;
        } else {
            
            $query = "UPDATE customer SET name='$name',
            email='$email', address='$address', zipcode='$zipcode', phone='$phone'
            WHERE id='$id'";
            $result = $this->db->update($query);
            if ($result) {
                
                $alert= "<span class='success'>Customer updated successfully</span>";
                return $alert;
            } else {
                $alert= "<span class='error'>Customer updated not successfully</span>";
                return $alert;
            }
        
            
        }
    }

    public function insertComment() {
        $nameCmt = $_POST['nameCmt'];
        $productID = $_POST['productIDCmt'];
        $comment = $_POST['comment'];

        $nameCmt = mysqli_real_escape_string($this->db->link, $nameCmt);
        $productID = mysqli_real_escape_string($this->db->link, $productID);
        $comment = mysqli_real_escape_string($this->db->link, $comment);

        if($nameCmt=='' || $comment=='') {
            $alert ="<span class='error'>Failed</span>";
            return $alert;
        } else {
            $query = "INSERT INTO comment(nameCmt, comment, productID)
                VALUES ('$nameCmt', '$comment', '$productID')";
                $result = $this->db->insert($query);
                if ($result) {
                    $alert= "<span class='success'>Successfully</span>";
                    return $alert;
                } else {
                    $alert= "<span class='error'>Failed</span>";
                    return $alert;
                }
        }
    }
}