<?php
    include_once "include/header.php";
?>

<?php 
    

    if (isset($_GET['deteleID'])) {
        $customerID = Session::get("customerID");
        $id = $_GET['deteleID'];
        $deleteWishlist = $product->deleteWishlist($id, $customerID);
    }


?>


<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="#">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Wishlist</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Cart Start -->
<div class="container-fluid">

    <div class="row px-xl-5">

        <div class="col-lg-8 table-responsive mb-5">
            <table class="table table-light table-borderless table-hover text-center mb-0">


                <thead class="thead-dark">
                    <tr>
                        <th>Products</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody class="align-middle">

                    <?php 
                        $id = Session::get("customerID");
                        $getWishlist = $product->getWishlist($id);
                        if($getWishlist) {
                            while($result=$getWishlist->fetch_assoc()) {
                                ?>
                    <tr>
                        <td class="align-middle"><?php echo $result['productName']?>
                        </td>
                        <td><img src="admin/upload/<?php echo $result['image']?>" alt="" style="width: 50px;"></td>
                        <td class="align-middle"><?php echo $result['price']?></td>

                        <td class="align-middle">
                            <button class="btn btn-sm btn-danger">
                                <a href="?deteleID=<?php echo $result['id']?>"><i class="fa fa-times"></i></a>
                            </button>
                        </td>
                    </tr>

                    <?php
                   
                            }
                        }
                    ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
<!-- Cart End -->


<?php
    include_once "include/footer.php";
?>