﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>


<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/category.php");
?>

<?php 

	$category = new Category();
	if (isset($_GET['deleteID']) && $_GET['deleteID'] != null) {
		$id = $_GET['deleteID'];
		$deleteCategory = $category->deleteCategory($id);
	}


?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Category List</h2>
        <?php 
            if(isset($deleteCategory)) {
                echo $deleteCategory;
            }
        ?>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Category Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
						$category = new Category();
						$getAllCategory = $category->showCategory();
						if($getAllCategory) {
							$i = 0;
							while($result = $getAllCategory->fetch_assoc()){
								$i++;	
					?>
                    <tr class="odd gradeX">
                        <td><?php echo $i?></td>
                        <td><?php echo $result['categoryName']?></td>
                        <td><a href="categoryedit.php?categoryID=<?php echo $result['id']?>">Edit</a> ||
                            <a onclick="return confirm('Are you want to delete?')"
                                href="?deleteID=<?php echo $result['id']?>">Delete</a>
                        </td>
                    </tr>

                    <?php 
							}
						}
					?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    setupLeftMenu();

    $('.datatable').dataTable();
    setSidebarHeight();
});
</script>
<?php include 'inc/footer.php';?>