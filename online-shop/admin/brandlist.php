<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>


<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/brand.php");
?>

<?php 

	$brand = new Brand();
	if (isset($_GET['deleteID']) && $_GET['deleteID'] != null) {
		$id = $_GET['deleteID'];
		$deletebrand = $brand->deletebrand($id);
	}


?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>brand List</h2>
        <?php 
            if(isset($deleteBrand)) {
                echo $deleteBrand;
            }
        ?>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>brand Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
						$brand = new Brand();
						$getAllBrand = $brand->showBrand();
						if($getAllBrand) {
							$i = 0;
							while($result = $getAllBrand->fetch_assoc()){
								$i++;	
					?>
                    <tr class="odd gradeX">
                        <td><?php echo $i?></td>
                        <td><?php echo $result['brandName']?></td>
                        <td><a href="brandedit.php?brandID=<?php echo $result['id']?>">Edit</a> ||
                            <a onclick="return confirm('Are you want to delete?')"
                                href="?deleteID=<?php echo $result['id']?>">Delete</a>
                        </td>
                    </tr>

                    <?php 
							}
						}
					?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    setupLeftMenu();

    $('.datatable').dataTable();
    setSidebarHeight();
});
</script>
<?php include 'inc/footer.php';?>