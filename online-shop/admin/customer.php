<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php
	$filepath = realpath(dirname(__FILE__));
	include_once ($filepath."/../classes/customer.php");
    include_once ($filepath."/../classes/cart.php");
	include_once ($filepath."/../helper/format.php");
?>

<?php

if (isset($_GET['customerID']) && $_GET['customerID'] != null) {
    $id = $_GET['customerID'];
    $order_code = $_GET['ordercode'];
} else {
    echo "<script>window.location.href='inbox.php'</script>";
}
$customer = new Customer();
$order = new Cart();



?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Information</h2>


        <?php
        $getCustomer = $customer->showCustomer($id);
        if ($getCustomer) {
            while ($result = $getCustomer->fetch_assoc()) {

                ?>


        <div class="block copyblock">
            <form method="POST">
                <table class="form">
                    <tr>
                        <td>Name</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['name'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['email'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['phone'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['address'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['city'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['country'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>Zip code</td>
                        <td>
                            <input readonly="readonly" value="<?php echo $result['zipcode'] ?>" type="text"
                                class="medium" />
                        </td>
                    </tr>

                </table>
            </form>
        </div>

        <?php

            }
        }
        ?>




        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name Product</th>
                        <th scope="col">price</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Pay</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                $getOrder = $order->showOrder($order_code);
                if ($getOrder) {
                    $i=0;
                    $subtotal=0;
                    $total=0;
                    while ($result = $getOrder->fetch_assoc()) {
                        $i++;
                        $subtotal = $result['quantity']*$result['price'];
                        $total += $subtotal;
                        ?>
                    <tr>
                        <th scope="row"><?php echo $i?></th>
                        <td><?php echo $result['productName']?></td>
                        <td><?php echo number_format($result['price'],0,',','.')?></td>
                        <td><?php echo $result['quantity']?></td>
                        <td><?php echo number_format($result['quantity']*$result['price'],0,',','.')?></td>
                    </tr>
                    <?php

                    }
                    }
                    ?>
                    <tr>
                        <td scope="col">Money: <?php echo number_format($total,0,',','.')?></td>
                    </tr>

                </tbody>
            </table>
        </div>

    </div>

</div>
<?php include 'inc/footer.php'; ?>