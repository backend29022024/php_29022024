<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . "/../classes/category.php");
?>

<?php
$category = new Category();

if (isset($_GET['categoryID']) && $_GET['categoryID'] != null) {
    $id = $_GET['categoryID'];
} else {
    echo "<script>window.location='categorylist.php'</script>";
}

if ($_SERVER['REQUEST_METHOD'] === "POST") {
    $categoryName = $_POST['categoryName'];

    $updateCategory = $category->updateCategory($id, $categoryName);
}

?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Edit Category</h2>

        <?php 
            if(isset($updateCategory)) {
                echo $updateCategory;
            }
        ?>

        <?php
        if (isset($insertCategory)) {
            echo $insertCategory;
        }
        ?>

        <?php
        $getCategory = $category->getCategoryById($id);
        if ($getCategory) {
            while ($result = $getCategory->fetch_assoc()) {

                ?>


        <div class="block copyblock">
            <form method="POST">
                <table class="form">
                    <tr>
                        <td>
                            <input name="categoryName" value="<?php echo $result['categoryName'] ?>" type="text"
                                placeholder="Enter Category Name..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>

        <?php

            }
        }
        ?>
    </div>
</div>
<?php include 'inc/footer.php'; ?>