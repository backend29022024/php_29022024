﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>


<?php 
	$filepath = realpath(dirname(__FILE__));
	include_once ($filepath."/../classes/cart.php");
	include_once ($filepath."/../helper/format.php");

	$cart = new Cart();

	if (isset($_GET['shiftID'])) {
        $id = $_GET['shiftID'];
		$shifted = $cart->shifted($id);
        if ($shifted) {
            echo "<script>window.location.href='inbox.php'</script>";
         }
    }

	if (isset($_GET['deleteShiftID'])) {
        $id = $_GET['deleteShiftID'];
		$deletedShifted = $cart->deleteShiftID($id);
        if ($deletedShifted) {
            echo "<script>window.location.href='inbox.php'</script>";
         }
    }

    
    
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Inbox</h2>
        <div class="block">
            <?php 
				if(isset($shifted)) {
					echo $shifted;
				}
			?>

            <?php 
				if(isset($deletedShifted)) {
					echo $deletedShifted;
				}
			?>
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Order time</th>
                        <th>Order code</th>
                        <th>CustomerID</th>
                        <th>Action</th>
                        <th>status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
						$format = new Format();
						$cart = new Cart();
						$getInbox = $cart->getInboxCart();
						if($getInbox) {
							$i =0;
							while($result= $getInbox->fetch_assoc()) {
								$i++;
								?>
                    <tr class="odd gradeX">
                        <td><?php echo $i?></td>
                        <td><?php echo $format->formatDate($result['order_created'])?></td>
                        <td><?php echo $result['order_code']?></td>
                        <td><?php echo $result['customer_id']?></td>
                        <td><a
                                href="customer.php?customerID=<?php echo $result['customer_id']?>&ordercode=<?php echo $result['order_code']?>">View
                                Order</a></td>
                        <td>
                            <?php 
								if($result['status']=='0'){
									?>
                            <a href="?shiftID=<?php echo $result['order_code']?>">Pending</a>
                            <?php 
								} else if($result['status']=='1'){
									?>
                            <a>Shifting</a>

                            <?php
								} else if($result['status']=='2') {
									?>
                            <a href="?deleteShiftID=<?php echo $result['order_code']?>">Remove</a>
                            <?php
								}
							?>
                        </td>
                    </tr>
                    <?php
							}
						}
					?>


                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    setupLeftMenu();

    $('.datatable').dataTable();
    setSidebarHeight();
});
</script>
<?php include 'inc/footer.php';?>