﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/category.php");
?>

<?php 
	$category = new Category();

	if($_SERVER['REQUEST_METHOD']==="POST") {
		$categoryName = $_POST['categoryName'];

		$insertCategory = $category->insertCategory($categoryName);
	}

?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Category</h2>
        <?php 
            if(isset($insertCategory)) {
                echo $insertCategory;
            }
        ?>
        <div class="block copyblock">
            <form method="POST">
                <table class="form">
                    <tr>
                        <td>
                            <input name="categoryName" type="text" placeholder="Enter Category Name..."
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php include 'inc/footer.php';?>