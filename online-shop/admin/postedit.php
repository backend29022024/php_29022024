<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/post.php");
?>

<?php 
	$post = new Post();

    if (isset($_GET['postID']) && $_GET['postID'] != null) {
        $id = $_GET['postID'];
    } else {
        echo "<script>window.location='postlist.php'</script>";
    }
    
    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        $title = $_POST['title'];
        $description = $_POST['description'];
        $status = $_POST['status'];
    
        $updatePost = $post->updatePost($id, $title, $description, $status);
    }

?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Edit Post</h2>
        <?php 
            if(isset($updatePost)) {
                echo $updatePost;
            }
        ?>

        <?php
        $getPostByID = $post->getPostByID($id);
        if ($getPostByID) {
            while ($result = $getPostByID->fetch_assoc()) {

                ?>

        <div class="block copyblock">
            <form method="POST">
                <table class="form">
                    <tr>
                        <td>
                            <input name="title" type="text" value="<?php echo $result['title'] ?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="description" type="text" value="<?php echo $result['description'] ?>"
                                class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="status" id="">
                                <option value="0">undisplay</option>
                                <option value="1">Display</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <?php 
        }}?>
    </div>
</div>
<?php include 'inc/footer.php';?>