<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . "/../classes/blog.php");
include_once($filepath . "/../helper/format.php");
?>

<?php
$blog = new Blog();
if (isset($_GET['deleteID']) && $_GET['deleteID'] != null) {
	$id = $_GET['deleteID'];
	$deleteBlog = $blog->deleteBlog($id);
}

?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Product List</h2>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Post</th>
                        <th>Content</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
					$getBlog = $blog->showBlog();
					$format = new Format();
					if ($getBlog) {
						while ($result = $getBlog->fetch_assoc()) {
							?>
                    <tr class="odd gradeX">
                        <td>
                            <?php echo $result['id'] ?>
                        </td>
                        <td>
                            <?php echo $result['blog_title'] ?>
                        </td>
                        <td><img width="50px" src="upload/<?php echo $result['image'] ?>"></td>
                        <td>
                            <?php echo $result['title'] ?>
                        </td>
                        <td>
                            <?php echo $result['content'] ?>
                        </td>

                        <td width="50%">
                            <?php echo $result['description'] ?>
                        </td>

                        <td class="center">
                            <?php
									if ($result['status'] == 0) {
										echo "Undisplay";
									} else {
										echo "Display";
									}
									?>
                        </td>
                        <td><a href="blogedit.php?ID=<?php echo $result['id']?>">Edit</a> || <a
                                href="?deleteID=<?php echo $result['id']?>">Delete</a></td>
                    </tr>
                    <?php
						}
					}
					?>


                </tbody>
            </table>

        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    setupLeftMenu();
    $('.datatable').dataTable();
    setSidebarHeight();
});
</script>
<?php include 'inc/footer.php'; ?>