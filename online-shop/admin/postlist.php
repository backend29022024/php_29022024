<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>


<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/post.php");
include_once ($filepath."/../helper/format.php");
?>

<?php 

	$post = new Post();
	if (isset($_GET['deleteID']) && $_GET['deleteID'] != null) {
		$id = $_GET['deleteID'];
		$deletePost = $post->deletePost($id);
	}


?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Category List</h2>
        <?php 
            if(isset($deleteCategory)) {
                echo $deleteCategory;
            }
        ?>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>Serial No.</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
						$post = new Post();
                        $format = new Format();
						$showPost = $post->showPost();
						if($showPost) {
							$i = 0;
							while($result = $showPost->fetch_assoc()){
								$i++;	
					?>
                    <tr class="odd gradeX">
                        <td><?php echo $i?></td>
                        <td><?php echo $result['title']?></td>
                        <td><?php echo $format->textShorten($result['description'], 100) ?></td>
                        <td><?php
                            if($result['status']=='0') {
                                echo 'Undisplay';
                            } else {
                                echo 'Display';
                            }
                        ?></td>
                        <td><a href="postedit.php?postID=<?php echo $result['id']?>">Edit</a> ||
                            <a onclick="return confirm('Are you want to delete?')"
                                href="?deleteID=<?php echo $result['id']?>">Delete</a>
                        </td>
                    </tr>

                    <?php 
							}
						}
					?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    setupLeftMenu();

    $('.datatable').dataTable();
    setSidebarHeight();
});
</script>
<?php include 'inc/footer.php';?>