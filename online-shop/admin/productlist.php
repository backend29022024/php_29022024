﻿<?php include 'inc/header.php'; ?>
<?php include 'inc/sidebar.php'; ?>

<?php
$filepath = realpath(dirname(__FILE__));
include_once($filepath . "/../classes/product.php");
include_once($filepath . "/../classes/brand.php");
include_once($filepath . "/../classes/category.php");
include_once($filepath . "/../helper/format.php");
?>

<?php
$product = new Product();
if (isset($_GET['deleteID']) && $_GET['deleteID'] != null) {
	$id = $_GET['deleteID'];
	$deleteProduct = $product->deleteProduct($id);
}

?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Product List</h2>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Product Name</th>
                        <th>Image</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
					$getproduct = $product->showProduct();
					$format = new Format();
					if ($getproduct) {
						while ($result = $getproduct->fetch_assoc()) {
							?>
                    <tr class="odd gradeX">
                        <td>
                            <?php echo $result['id'] ?>
                        </td>
                        <td>
                            <?php echo $result['productName'] ?>
                        </td>
                        <td><img width="50px" src="upload/<?php echo $result['image'] ?>"></td>
                        <td><?php echo $result['product_quantity']?></td>
                        <td>
                            <?php echo $result['price'] . 'đ' ?>
                        </td>
                        <td>
                            <?php echo $result['categoryName'] ?>
                        </td>
                        <td>
                            <?php echo $result['brandName'] ?>
                        </td>
                        <td>
                            <?php echo $format->textShorten($result['description'], 50) ?>
                        </td>
                        <td class="center">
                            <?php
									if ($result['type'] == 0) {
										echo "Non-feathered";
									} else {
										echo "Feathered";
									}
									?>
                        </td>
                        <td><a href="productedit.php?productID=<?php echo $result['id']?>">Edit</a> || <a
                                href="?deleteID=<?php echo $result['id']?>">Delete</a></td>
                    </tr>
                    <?php
						}
					}
					?>


                </tbody>
            </table>

        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    setupLeftMenu();
    $('.datatable').dataTable();
    setSidebarHeight();
});
</script>
<?php include 'inc/footer.php'; ?>