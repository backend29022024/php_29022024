<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/post.php");
?>

<?php 
	$post = new Post();

	if($_SERVER['REQUEST_METHOD']==="POST") {
		$title = $_POST['title'];
        $description = $_POST['description'];
        $status = $_POST['status'];

		$insertPost = $post->insertPost($title, $description, $status);
	}

?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Category</h2>
        <?php 
            if(isset($insertPost)) {
                echo $insertPost;
            }
        ?>
        <div class="block copyblock">
            <form method="POST">
                <table class="form">
                    <tr>
                        <td>
                            <input name="title" type="text" placeholder="Enter title post..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input name="description" type="text" placeholder="Enter description..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="status" id="">
                                <option value="0">undisplay</option>
                                <option value="1">Display</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>
<?php include 'inc/footer.php';?>