<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/product.php");
include_once ($filepath."/../classes/brand.php");
include_once ($filepath."/../classes/category.php");
?>

<?php 
	$product = new Product();
    if (isset($_GET['productID']) && $_GET['productID'] != null) {
        $id = $_GET['productID'];
    } else {
        echo "<script>window.location='productlist.php'</script>";
    }
    
    if ($_SERVER['REQUEST_METHOD'] === "POST") {
        if($_SERVER['REQUEST_METHOD']==="POST" && $_POST['submit']) {
            $updateProduct = $product->updateProduct($_POST, $_FILES, $id);
        }
    }

?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Edit Product</h2>
        <?php 
            if(isset($updateProduct)){
                echo $updateProduct;
            }
        ?>
        <div class="block">
            <?php 
                $getProductById = $product->getProductById($id);
                if($getProductById) {
                    while ($result = $getProductById->fetch_assoc()) {
                        ?>
            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Name</label>
                        </td>
                        <td>
                            <input type="text" value="<?php echo $result['productName']?>" name="productName"
                                placeholder="Enter Product Name..." class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="">quantity</label>
                        </td>
                        <td>
                            <input type="number" name="quantity" min="1"
                                value="<?php echo $result['product_quantity']?>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="categoryID">
                                <option>Select Category</option>
                                <?php 
                                $category = new Category();
                                $categoryList = $category->showCategory();
                                if($categoryList) {
                                    while($resultCategory =$categoryList->fetch_assoc()) {
                                        ?>
                                <option <?php 
                                        if($result['categoryID']== $resultCategory['id']) {
                                            echo "selected";
                                        }
                                    ?> value="<?php echo $resultCategory['id']?>">
                                    <?php echo $resultCategory['categoryName']?></option>

                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Brand</label>
                        </td>
                        <td>
                            <select id="select" name="brandID">
                                <option>Select Brand</option>
                                <?php 
                                $brand = new Brand();
                                $brandList = $brand->showBrand();
                                if($brandList) {
                                    while($resultBrand =$brandList->fetch_assoc()) {
                                        ?>
                                <option <?php 
                                        if($result['brandID'] == $resultBrand['id']) {
                                            echo "selected";
                                        }
                                    ?> value="<?php echo $resultBrand['id']?>"><?php echo $resultBrand['brandName']?>
                                </option>

                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Description</label>
                        </td>
                        <td>
                            <textarea name="description"
                                class="tinymce"> <?php echo $result['description']?> </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Price</label>
                        </td>
                        <td>
                            <input value="<?php echo $result['price']?>" name="price" type="text"
                                placeholder="Enter Price..." class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <img width="80px" src="upload/<?php echo $result['image'] ?>">
                            <br>
                            <input name="image" type="file" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Product Type</label>
                        </td>
                        <td>
                            <select id="select" name="type">
                                <option>Select Type</option>
                                <?php 
                                    if($result['type']==0)
                                    {
                                        ?>
                                <option value="1">Featured</option>
                                <option selected value="0">Non-Featured</option>
                                <?php
                                    } else {
                                        ?>
                                <option selected value="1">Featured</option>
                                <option value="0">Non-Featured</option>
                                <?php
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update" />
                        </td>
                    </tr>
                </table>
            </form>
            <?php
                    }
                }
            ?>

        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    setupTinyMCE();
    setDatePicker('date-picker');
    $('input[type="checkbox"]').fancybutton();
    $('input[type="radio"]').fancybutton();
});
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>