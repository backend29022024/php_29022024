<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>

<?php 
$filepath = realpath(dirname(__FILE__));
include_once ($filepath."/../classes/blog.php");
include_once ($filepath."/../classes/post.php");
?>

<?php 
	$blog = new Blog();

    if (isset($_GET['ID']) && $_GET['ID'] != null) {
        $id = $_GET['ID'];
    }

	if($_SERVER['REQUEST_METHOD']==="POST" && $_POST['submit']) {
		$updateBlog = $blog->updateBlog($_POST, $_FILES, $id);
	}

?>

<div class="grid_10">
    <div class="box round first grid">
        <h2>Add New Blog</h2>
        <?php 
            if(isset($updateBlog)){
                echo $updateBlog;
            }
        ?>
        <div class="block">
            <?php 
                $getBlogByID =  $blog->getBlogById($id);
                if($getBlogByID) {
                    while($result = $getBlogByID->fetch_assoc()) {
                        ?>
            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Title</label>
                        </td>
                        <td>
                            <input type="text" name="title" value="<?php echo $result['blog_title']?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Post</label>
                        </td>
                        <td>
                            <select id="select" name="post">
                                <option>Select Post</option>
                                <?php 
                                $post = new Post();
                                $postList = $post->showPost();
                                if($postList) {
                                    while($resultPost =$postList->fetch_assoc()) {
                                        ?>
                                <option <?php
                                            if($result['post'] == $resultPost['id']) {
                                                echo 'selected';
                                            } else {
                                                echo '';
                                            }
                                        ?> value="<?php echo $resultPost['id']?>"><?php echo $resultPost['title']?>
                                </option>

                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Description</label>
                        </td>
                        <td>
                            <textarea name="description" class="tinymce"><?php echo $result['description']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Content</label>
                        </td>
                        <td>
                            <input name="content" type="text" value="<?php echo $result['content']?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <input name="image" type="file" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Status</label>
                        </td>
                        <td>
                            <select id="select" name="status">
                                <option>Select Type</option>
                                <?php  
                                    if($result['status'] == '0') {
                                        ?>
                                <option value="1">Display</option>
                                <option selected value="0">Undisplay</option>
                                <?php
                                    } else {
                                        ?>
                                <option selected value="1">Display</option>
                                <option value="0">Undisplay</option>
                                <?php
                                    }
                                ?>

                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Save" />
                        </td>
                    </tr>
                </table>
            </form>
            <?php
                    }
                }
            ?>

        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
    setupTinyMCE();
    setDatePicker('date-picker');
    $('input[type="checkbox"]').fancybutton();
    $('input[type="radio"]').fancybutton();
});
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>