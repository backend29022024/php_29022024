<?php
    include_once "include/header.php";
?>

<?php 
    $loginCheck = Session::get('customerLogin');
    if(!$loginCheck) {
        echo "<script>window.location.href='login.php'</script>";
    }
?>

<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="index.php">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Payment</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Checkout Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <h3>Choose your method parment</h3>
        <a href="offlinepayment.php">Offline Payment</a>
        <p>||</p>
        <a href="donhangthanhtoanonline.php">Online Payment</a>
    </div>
</div>
<!-- Checkout End -->

<?php
    include_once "include/footer.php";
?>