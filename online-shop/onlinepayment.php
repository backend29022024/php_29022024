<?php
    include_once "include/header.php";
?>

<?php 
    $loginCheck = Session::get('customerLogin');
    if(!$loginCheck) {
        echo "<script>window.location.href='login.php'</script>";
    }
?>

<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="index.php">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Payment</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Checkout Start -->
<div class="container-fluid">
    <h3>Choose your method parment</h3>
    <div class="row px-xl-5">
        <div>

            <br />
            <a href="donhangthanhtoanonline.php">
                <button class="btn btn-success">VNPAY</button>
            </a>
        </div>
    </div>
</div>
<!-- Checkout End -->

<?php
    include_once "include/footer.php";
?>