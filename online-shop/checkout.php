<?php
    include_once "include/header.php";
?>

<?php 
    $loginCheck = Session::get('customerLogin');
    if(!$loginCheck) {
        echo "<script>window.location.href='login.php'</script>";
    }
?>

<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="#">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Checkout</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Checkout Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-lg-7">
            <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Billing
                    Address</span></h5>
            <div class="bg-light p-30 mb-5">
                <?php 
                    $id = Session::get("customerID");
                    $showCustomer = $customer->showCustomer($id);
                    if($showCustomer) {
                        while($result = $showCustomer->fetch_assoc()) {
                            ?>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>Name</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['name']?>">
                    </div>

                    <div class="col-md-6 form-group">
                        <label>E-mail</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['email']?>">
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Mobile No</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['phone']?>">
                    </div>

                    <div class="col-md-6 form-group">
                        <label>Address Line 2</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['address']?>">
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Country</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['country']?>">
                    </div>
                    <div class="col-md-6 form-group">
                        <label>City</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['city']?>">
                    </div>

                    <div class="col-md-6 form-group">
                        <label>ZIP Code</label>
                        <input disabled class="form-control" type="text" value="<?php echo $result['zipcode']?>">
                    </div>


                </div>
                <?php
                        }
                    }
                ?>

            </div>

        </div>
        <div class="col-lg-5">
            <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Order
                    Total</span></h5>
            <div class="bg-light p-30 mb-5">
                <div class="border-bottom">
                    <h6 class="mb-3">Products</h6>
                    <?php 
                        $getCart = $cart->getCart();
                        if($getCart) {
                            $total = 0;
                            while($result = $getCart->fetch_assoc()) {
                                ?>
                    <div class="d-flex justify-content-between">
                        <p><?php echo $result['productName']?></p>
                        <p><?php echo $result['quantity']?></p>
                        <p><?php $subtotal = $result['price']*$result['quantity']; echo $subtotal.'VND'?></p>
                    </div>
                    <?php
                            $total += $subtotal;
                            }
                        }
                    ?>


                </div>
                <?php
                $checkCart = $cart->checkCart();
                if($checkCart) {
                    ?>
                <div class="border-bottom pt-3 pb-2">
                    <div class="d-flex justify-content-between mb-3">
                        <h6>Subtotal</h6>
                        <h6><?php 
                                echo $total;
                            ?></h6>
                    </div>
                    <div class="d-flex justify-content-between">
                        <h6 class="font-weight-medium">Shipping</h6>
                        <h6 class="font-weight-medium">0</h6>
                    </div>
                </div>
                <div class="pt-2">
                    <div class="d-flex justify-content-between mt-2">
                        <h5>Total</h5>
                        <h5>
                            <?php 
                            echo $total;
                            Session::set('sum', $total);
                        ?>
                        </h5>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
            <div class="mb-5">
                <h5 class="section-title position-relative text-uppercase mb-3"><span
                        class="bg-secondary pr-3">Payment</span></h5>
                <div class="bg-light p-30">
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="payment" id="paypal">
                            <label class="custom-control-label" for="paypal">Paypal</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="payment" id="directcheck">
                            <label class="custom-control-label" for="directcheck">Direct Check</label>
                        </div>
                    </div>
                    <div class="form-group mb-4">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" name="payment" id="banktransfer">
                            <label class="custom-control-label" for="banktransfer">Bank Transfer</label>
                        </div>
                    </div>
                    <button class="btn btn-block btn-primary font-weight-bold py-3">Place Order</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Checkout End -->

<?php
    include_once "include/footer.php";
?>