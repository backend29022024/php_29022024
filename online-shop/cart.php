<?php
    include_once "include/header.php";
?>

<?php 
    if($_SERVER['REQUEST_METHOD']==='POST' && isset($_POST['submit'])) {
        $quantity = $_POST['quantity'];
        $stock = $_POST['stock'];
        $id = $_POST['cartID'];
        if($quantity<=0) {
            $deleteCart = $cart->deleteCart($id);
        } else {
            $updateQuantity = $cart->updateQuantity($id, $quantity, $stock);
        }
    }

    if (isset($_GET['cartID']) && $_GET['cartID'] != null) {
        $id = $_GET['cartID'];
        $deleteCart = $cart->deleteCart($id);
    }

    if(!isset($_GET['id'])) {
        echo "<meta http-equiv='refresh' content='0;URL=?id=live'>";
    }

?>


<!-- Breadcrumb Start -->
<div class="container-fluid">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-dark" href="#">Home</a>
                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                <span class="breadcrumb-item active">Shopping Cart</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->


<!-- Cart Start -->
<div class="container-fluid">

    <div class="row px-xl-5">

        <div class="col-lg-8 table-responsive mb-5">
            <table class="table table-light table-borderless table-hover text-center mb-0">
                <?php 
                    if(isset($updateQuantity)){
                        echo $updateQuantity;
                    }
                    if(isset($deleteCart)) {
                        echo $deleteCart;
                    }
                ?>


                <thead class="thead-dark">
                    <tr>
                        <th>Buy</th>
                        <th>Products</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody class="align-middle">

                    <?php 
                        $getCart = $cart->getCart();
                        if($getCart) {
                            $subtotal = 0;
                            $quantity = 0;
                            while($result=$getCart->fetch_assoc()) {
                                ?>
                    <tr>
                        <td><input <?php echo $result['check_buy']==1?'checked':'' ?> type="checkbox"
                                value="<?php echo $result['id']?>" name="check_buy" class="form-check-input check_buy">
                        </td>
                        <td class="align-middle"><?php echo $result['productName']?>
                        </td>
                        <td><img src="admin/upload/<?php echo $result['image']?>" alt="" style="width: 50px;"></td>
                        <td class="align-middle"><?php echo $result['price']?></td>
                        <td class="align-middle"><?php echo $result['stock']?></td>
                        <td class="align-middle">
                            <!-- <div class="input-group quantity mx-auto" style="width: 100px;">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-minus">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
                                <input type="text"
                                    class="form-control form-control-sm bg-secondary border-0 text-center" value="1">
                                <div class="input-group-btn">
                                    <button class="btn btn-sm btn-primary btn-plus">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div> -->
                            <form action="" method="post">
                                <input type="hidden" name="cartID" value="<?php echo $result['id']?>">
                                <input type="hidden" name="stock" value="<?php echo $result['stock']?>">
                                <input type="number" name="quantity" value="<?php echo $result['quantity']?>">
                                <input type="submit" name="submit" value="Update">
                            </form>
                        </td>
                        <td class="align-middle">
                            <?php 
                                $total = $result['price']*$result['quantity'];
                                echo $total;
                            ?>

                        </td>
                        <td class="align-middle">
                            <button class="btn btn-sm btn-danger">
                                <a href="?cartID=<?php echo $result['id']?>"><i class="fa fa-times"></i></a>
                            </button>
                        </td>
                    </tr>

                    <?php
                    $subtotal += $total;
                    $quantity = $quantity + $result['quantity'];
                            }
                        }
                    ?>

                </tbody>
            </table>
        </div>
        <div class="col-lg-4">
            <form class="mb-30" action="">
                <div class="input-group">
                    <input type="text" class="form-control border-0 p-4" placeholder="Coupon Code">
                    <div class="input-group-append">
                        <button class="btn btn-primary">Apply Coupon</button>
                    </div>
                </div>
            </form>
            <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Cart
                    Summary</span></h5>
            <?php 
                $checkCart = $cart->checkCart();
                if($checkCart) {
                    ?>
            <div class="bg-light p-30 mb-5">
                <div class="border-bottom pb-2">
                    <div class="d-flex justify-content-between mb-3">
                        <h6>Subtotal</h6>
                        <h6><?php 
                            echo $subtotal;
                            Session::set('sum', $subtotal);
                            Session::set('quantity', $quantity);
                        ?></h6>
                    </div>
                    <div class="d-flex justify-content-between">
                        <h6 class="font-weight-medium">Shipping</h6>
                        <h6 class="font-weight-medium">0</h6>
                    </div>
                </div>
                <div class="pt-2">
                    <div class="d-flex justify-content-between mt-2">
                        <h5>Total</h5>
                        <h5><?php 
                            echo $subtotal;
                        ?></h5>
                    </div>
                    <a href="payment.php">
                        <button class="btn btn-block btn-primary font-weight-bold my-3 py-3">Proceed To
                            Checkout</button>
                    </a>

                </div>
            </div>
            <?php
                } else {
                    echo "Empty!!!";
                }
            ?>
        </div>
    </div>
</div>
<!-- Cart End -->


<?php
    include_once "include/footer.php";
?>