<?php
    include_once "include/header.php";
?>

<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
</head>
<style>
body {
    text-align: center;
    padding: 40px 0;
    background: #EBF0F5;
}

h1 {
    color: #88B04B;
    font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
    font-weight: 900;
    font-size: 40px;
    margin-bottom: 10px;
}

p {
    color: #404F5E;
    font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
    font-size: 20px;
    margin: 0;
}

.checkmark {
    color: #9ABC66;
    font-size: 100px;
    line-height: 200px;
    margin-left: -15px;
}

.card_success {
    background: white;
    padding: 60px;
    border-radius: 4px;
    box-shadow: 0 2px 3px #C8D0D8;
    display: inline-block;
    margin: 0 auto;
}
</style>

<body>
    <div class="card_success">
        <div style="border-radius:200px; height:200px; width:200px; background: #F8FAF5; margin:0 auto;">
            <i class="checkmark">✓</i>
        </div>
        <?php 
            $id = Session::get("customerID");
            $getAmount = $cart->getAmountPrice($id);
            if($getAmount) {
                $amount = 0;
                while($result=$getAmount->fetch_assoc()) {
                    $price = $result['price'];
                    $amount += $price;
                }
            }
        ?>
        <h1>Success</h1>
        <p>Total price: <?php echo $amount?> VND<br /> See your order details <a href="orderdetail.php">here</a></p>
    </div>

</body>

</html>

<?php
    include_once "include/footer.php";
?>